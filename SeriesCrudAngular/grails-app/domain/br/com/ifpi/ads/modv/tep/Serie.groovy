package br.com.ifpi.ads.modv.tep

import grails.rest.Resource;

@Resource(uri = "/series", formats = ['json'])
class Serie {
	
	String titulo;
	String sinopse;
	String ano;
	String genero;

	static mapping = {
		sinopse type : 'text'
	}
}
